package sample;

import com.google.gson.Gson;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

public class Controller {

    private String a = "";
    private String b = "";
    private double sum = 0;
    private String diya = "";
    private boolean clear_a = false;


    @FXML
    private Label lbl;

    @FXML
    private void clickOne() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
             a=a+"1";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"1";

                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }
            }
        }
    }



    @FXML
    private void clickTwo() {
        if(clear_a){a="";
        clear_a=false;}
        if (diya.equals("")){
            a=a+"2";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"2";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickThree() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"3";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"3";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickFour() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"4";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"4";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }
            }
        }
    }

    @FXML
    private void clickFive() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"5";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"5";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickSix() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"6";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"6";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickSeven() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"7";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"7";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickEight() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"8";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"8";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }}}
    }

    @FXML
    private void clickNine() {
        if(clear_a){a="";
            clear_a=false;}
        if (diya.equals("")){
            a=a+"9";
            lbl.setText(a);}
        else{
            if(!a.equals("")){
                b=b+"9";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }
            }
        }

    }

    @FXML
    private void clickZero() {
        if (!a.equals("")) {
            if (diya.equals("")) {
                a = a + "0";
                lbl.setText(a);
            } else {
                b = b + "0";
                if (Double.parseDouble(a) % 1 == 0) {
                    lbl.setText((new Double(a)).longValue()+" "+diya+" "+ b);
                } else {
                    lbl.setText(a+" "+diya+" "+b);
                }
            }
        }
    }

    @FXML
    private void clickPlusMinus(){
        if (!a.equals("")) {
            if (diya.equals("")) {
                if(!(String.valueOf(a.charAt(0))).equals("-")){
                    a="-"+a;
                    lbl.setText(a);
                }
                else{
                    a = a.substring(1);
                    lbl.setText(a);
                }
            } else {
                if(!(String.valueOf(b.charAt(0))).equals("-")){
                    b="-"+b;
                    lbl.setText(a+" "+diya+" "+b);
                }
                else{
                    b = b.substring(1);
                    lbl.setText(a+" "+diya+" "+b);
                }
            }
        }
    }

    @FXML
    private void clickProcent() throws IOException {
        if (diya.equals("")) {
            reset();
        } else{
            double double_b = Double.parseDouble(b);
            double double_a = Double.parseDouble(a);
            b = Double.toString((double_a*double_b)/100);

            if (Double.parseDouble(b) % 1 == 0) {
                lbl.setText(a+" "+diya+" "+(new Double(b)).longValue());
            } else {
                lbl.setText(a+" "+diya+" "+b);
            }
        }

    }

    @FXML
    private void plus() {
        diya = "+";
        clear_a=false;
        if (!a.equals("")) {
            if (Double.parseDouble(a) % 1 == 0) {
                lbl.setText((new Double(a)).longValue()+""+" + "+b);
            } else {
                lbl.setText(a+" "+" +");
            }
        }
    }

    @FXML
    private void minus() {
        diya = "-";
        clear_a=false;
        if (!a.equals("")) {
            if (Double.parseDouble(a) % 1 == 0) {
                lbl.setText((new Double(a)).longValue()+"" +" - "+b);
            } else {
                lbl.setText(a+" "+" -");
            }
        }
    }

    @FXML
    private void mnozh() {
        diya = "*";
        clear_a=false;
        if (!a.equals("")) {
            if (Double.parseDouble(a) % 1 == 0) {
                lbl.setText((new Double(a)).longValue() +"" +" * "+b);
            } else {
                lbl.setText(a+" "+" *");
            }
        }}

    @FXML
    private void dil() {
        diya = "/";
        clear_a=false;
        if (!a.equals("")) {
            if (Double.parseDouble(a) % 1 == 0) {
                lbl.setText((new Double(a)).longValue() +"" +" / "+b);
            } else {
                lbl.setText(a+" "+" /");
            }
        }
    }



    @FXML
    private void sum() {
        if(!a.equals("") & !b.equals("")) {
            switch (diya) {
                case "+":
                    sum = Double.parseDouble(a) + Double.parseDouble(b);

                    if(sum % 1==0){
                        lbl.setText(Long.toString((new Double(sum)).longValue()));
                        break;}
                    else {
                        lbl.setText(Double.toString(sum));
                        break;}

                case "-":
                    sum = Double.parseDouble(a) - Double.parseDouble(b);

                    if(sum % 1==0){
                        lbl.setText(Long.toString((new Double(sum)).longValue()));
                        break;}
                    else {
                        lbl.setText(Double.toString(sum));
                        break;}

                case "*":
                    sum = Double.parseDouble(a) * Double.parseDouble(b);

                    if(sum % 1==0 ){

                        lbl.setText(Long.toString((new Double(sum)).longValue()));
                        break;}
                    else {
                        lbl.setText(Double.toString(sum));
                        break;}
                case "/":
                    if (b.equals("0")) {
                        lbl.setStyle("-fx-font-size: 15px;");
                        lbl.setText("Не можна ділити на нуль!");
                        diya="";
                        a="";
                        b="";
                        break;
                    }
                    sum = Double.parseDouble(a) / Double.parseDouble(b);

                    if(sum % 1==0 | sum==0){
                        lbl.setText(Long.toString((new Double(sum)).longValue()));
                    break;}
                    else {
                        lbl.setText(Double.toString(sum));
                        break;}
            }
        }


        if(sum % 1==0 | sum==0){
            a=Long.toString((new Double(sum)).longValue());
            }
        else {
            a=Double.toString(sum);
            }
        b="";
        diya="";
        clear_a=true;
        if(sum>9223372036854775806.0){
            lbl.setStyle("-fx-font-size: 15px;");
            lbl.setText("Число занадто велике.");
        }
        if(sum<-9223372036854775806.0){
            lbl.setStyle("-fx-font-size: 15px;");
            lbl.setText("Число занадто мале.");
        }

    }

    @FXML
    private void reset() {
        lbl.setStyle("-fx-font-size: 20px;");
        lbl.setStyle("-fx-font-weight: bold");
        lbl.setText("0");
        diya = "";
        a = "";
        b = "";
        clear_a = false;
    }

    @FXML
    private void getCourse() throws IOException {
        //TEST JSON
        String url = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";

        HttpURLConnection httpClient =
                (HttpURLConnection) new URL(url).openConnection();

        // optional default is GET
        httpClient.setRequestMethod("GET");

        //add request header
        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = httpClient.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(httpClient.getInputStream()))) {

            StringBuilder response = new StringBuilder();
            String line;

            while ((line = in.readLine()) != null) {
                response.append(line);
            }

            //print result
            String mas = response.toString();
            System.out.println(mas);

            Gson gson = new Gson();
            Response[] str = gson.fromJson(mas, Response[].class);

            for(Response str1:str){
                if(str1.ccy.equals("USD")){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Курс долара Приватбанка");

                    alert.setHeaderText("Банк купляє долари по цьому курсу: "+str1.buy+"\nБанк продає долари по цьому курсу: "+str1.sale);
                    alert.show();

                }
            }
        }
    }
}
