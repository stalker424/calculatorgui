package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/sample.fxml")));
        primaryStage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("/App.png"))));
        primaryStage.setTitle("Калькулятор");
        primaryStage.setScene(new Scene(root, 250, 300));
        primaryStage.setMinHeight(300);
        primaryStage.setMaxHeight(300);
        primaryStage.setMinWidth(250);
        primaryStage.setMaxWidth(250);
        primaryStage.setWidth(250);
        primaryStage.setHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
